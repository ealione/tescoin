import pickle
from random import randint
from threading import Thread

import msgpack


class CachedProperty(object):
    """A decorator that converts a function into a lazy property.  The
    function wrapped is called the first time to retrieve the result
    and then that calculated result is used the next time you access
    the value::
        class Foo(object):
            @CachedProperty
            def foo(self):
                # calculate something important here
                return 42
    The class has to have a `__dict__` in order for this property to
    work.
    """

    # implementation detail: this property is implemented as non-data
    # descriptor.  non-data descriptors are only invoked if there is
    # no entry with the same name in the instance's __dict__.
    # this allows us to completely get rid of the access function call
    # overhead.  If one choses to invoke __get__ by hand the property
    # will still work as expected because the lookup logic is replicated
    # in __get__ for manual invocation.

    def __init__(self, func, name=None, doc=None):
        self.__name__ = name or func.__name__
        self.__module__ = func.__module__
        self.__doc__ = doc or func.__doc__
        self.func = func

    def __get__(self, obj, type=None):
        if obj is None:
            return self
        value = obj.__dict__.get(self.__name__, _missing)
        if value is _missing:
            value = self.func(obj)
            obj.__dict__[self.__name__] = value
        return value


class _Missing(object):

    def __repr__(self):
        return 'no value'

    def __reduce__(self):
        return '_missing'


_missing = _Missing()


def readable_bytes(nbytes: float) -> str:
    for x in ['B', 'KB', 'MB', 'GB', 'TB', 'PB']:
        if nbytes < 1024.:
            return f'{nbytes:.2f} {x}'
        nbytes /= 1024.


transports = frozenset(['udp', 'tcp', 'ipc', 'inproc'])


def zmq_addr(port, transport=None, host=None):
    if host is None:
        host = '127.0.0.1'

    if transport is None:
        transport = 'tcp'

    assert transport in transports
    assert 1000 < port < 10000

    return f'{transport}://{host}:{port}'


def threaded(fn):
    def wrapper(*args, **kwargs):
        Thread(target=fn, args=args, kwargs=kwargs).start()

    return wrapper


def rand_id():
    return u"%04x-%04x" % (randint(0, 0x10000), randint(0, 0x10000))


def split_address(msg):
    """Function to split return Id and message received by ROUTER socket.

    Returns 2-tuple with return Id and remaining message parts.
    Empty frames after the Id are stripped.
    """
    ret_ids = []
    for i, p in enumerate(msg):
        if p:
            ret_ids.append(p)
        else:
            break
    return ret_ids, msg[i + 1:]


def serialize_object(obj, protocol=-1):
    p = pickle.dumps(obj, protocol)
    return msgpack.packb(p)


def deserialize_object(msg):
    p = msgpack.unpackb(msg)
    return pickle.loads(p)
