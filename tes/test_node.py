from unittest import TestCase

from _exceptions import TransactionInsertionException
from config import TR_REWARD
from node import Node
from coin.payment import Payment


class TestNode(TestCase):
    def test_simple_payment(self):
        print("==================================Simple payment test.")
        print('Creating 2 nodes.')
        node1 = Node('TES')
        node2 = Node('ZAN')

        print("Node 1 mines a new block and notifies all other nodes.")
        node1.mine()
        node2.blockchain.insert_block(node1.blockchain.last_block)
        print("Node 1 should have gotten the 100 coin reward from mining.")
        self.assertEqual(node1.get_balance(), TR_REWARD)

        print("Node 1 pays Node 2 10 coins, with a 5 coin transaction fee.")
        transaction = node1.transaction([Payment(node2.wallet.public_key, 10, 5)])
        node2.transaction_tree.insert_transaction(transaction)
        self.assertEqual(len(node2.transaction_tree), 1)

        print("Node 2 mines a new block with the transaction.")
        node2.mine()
        print("Node 2 should have 115 coins.")
        self.assertEqual(node2.get_balance(), 115)

        print("Node 1 should have 85 coins.")
        node1.blockchain.insert_block(node2.blockchain.last_block)
        self.assertEqual(node1.get_balance(), 85)
        print("==================================================\n")

    def test_payment_error(self):
        print("==================================Payment error test.")
        print('Creating 2 nodes.')
        node1 = Node('TES')
        node2 = Node('ZAN')

        print("Node 1 mines a new block for a 100 coin reward.")
        node1.mine()
        print("Node 1 tries to transfer 200 coins to Node 2 and fails.")
        transaction = node1.transaction([Payment(node2.wallet.public_key, 200, 5)])
        self.assertRaises(TransactionInsertionException, node1.transaction_tree.insert_transaction, transaction)
        print("==================================================\n")

    def test_complex_payment(self):
        print("==================================Complex payment test.")
        print('Creating 7 nodes.')
        node1 = Node('TES')
        node2 = Node('ZAN')
        node3 = Node('RAL')
        node4 = Node('MAK')
        node5 = Node('BON')
        node6 = Node('DEA')
        node7 = Node('SAR')

        print("Node 1 mines one new block...")
        node1.mine()

        print("and notifies all other nodes.")
        node2.blockchain.insert_block(node1.blockchain.last_block)
        node3.blockchain.insert_block(node1.blockchain.last_block)
        node4.blockchain.insert_block(node1.blockchain.last_block)
        node5.blockchain.insert_block(node1.blockchain.last_block)
        node6.blockchain.insert_block(node1.blockchain.last_block)
        node7.blockchain.insert_block(node1.blockchain.last_block)

        print('Node 1 pays all other nodes 10 coins, each transaction incurs a 1 coin fee.')
        p1 = Payment(node2.wallet.public_key, 10, 1)
        p2 = Payment(node3.wallet.public_key, 10, 1)
        p3 = Payment(node4.wallet.public_key, 10, 1)
        p4 = Payment(node5.wallet.public_key, 10, 1)
        p5 = Payment(node6.wallet.public_key, 10, 1)
        p6 = Payment(node7.wallet.public_key, 10, 1)

        transaction = node1.transaction([p1, p2, p3, p4, p5, p6])
        node2.transaction_tree.insert_transaction(transaction)
        self.assertEqual(len(node2.transaction_tree), 1)

        print("Node 2 mines a new block with the transaction...")
        node2.mine()

        print("and notifies all other nodes.")
        node1.blockchain.insert_block(node2.blockchain.last_block)
        node3.blockchain.insert_block(node2.blockchain.last_block)
        node4.blockchain.insert_block(node2.blockchain.last_block)
        node5.blockchain.insert_block(node2.blockchain.last_block)
        node6.blockchain.insert_block(node2.blockchain.last_block)
        node7.blockchain.insert_block(node2.blockchain.last_block)

        print("Node 1 should have 34 coins.")
        self.assertEqual(node1.get_balance(), 34)
        print("Node 2 should have 116 coins.")
        self.assertEqual(node2.get_balance(), 116)
        print("Node 3 should have 10 coins.")
        self.assertEqual(node3.get_balance(), 10)
        print("Node 4 should have 10 coins.")
        self.assertEqual(node4.get_balance(), 10)
        print("Node 5 should have 10 coins.")
        self.assertEqual(node5.get_balance(), 10)
        print("Node 6 should have 10 coins.")
        self.assertEqual(node6.get_balance(), 10)
        print("==================================================\n")

    def test_double_spent(self):
        print("==================================Double spending test.")
        print('Creating 3 nodes.')
        node1 = Node('TES')
        node2 = Node('ZAN')
        node3 = Node('RAL')

        print("Node 1 mines one new block...")
        node1.mine()

        print("and notifies all other nodes.")
        node2.blockchain.insert_block(node1.blockchain.last_block)
        node3.blockchain.insert_block(node1.blockchain.last_block)

        print("Node 1 create a transaction for a payment of 10 coins to Node 2, with a one coin fee.")
        tx1 = node1.transaction([Payment(node2.wallet.public_key, 10, 1)])
        print("Node 1 create a transaction for a payment of 10 coins to Node 3, with a one coin fee.")
        tx2 = node1.transaction([Payment(node3.wallet.public_key, 10, 1)])

        print("Inserting the first transaction to Node 1 should succeed.")
        node1.transaction_tree.insert_transaction(tx1)
        print("Inserting the second transaction to Node 1 should fail.")
        self.assertRaises(TransactionInsertionException, node1.transaction_tree.insert_transaction, tx2)

        print("Node 1 mines a new block and notifies all other nodes.")
        node1.mine()
        node2.blockchain.insert_block(node1.blockchain.last_block)
        node3.blockchain.insert_block(node1.blockchain.last_block)

        print("There should be three transaction on the last block of Node 1.")
        self.assertEqual(len(node1.blockchain.last_block), 3)
        print("The first transaction should be the transaction with Node 2.")
        self.assertEqual(node1.blockchain.last_block.data[0], tx1)
        print("Node 2 should have no pending transactions.")
        self.assertEqual(len(node2.transaction_tree), 0)
        print("==================================================\n")

    def test_difficulty_change(self):
        print("==================================Difficulty test.")
        print("Creating a node.")
        node1 = Node('TES')
        print("Mining 26 times.")
        for _ in range(26):
            node1.mine()
        print("The node should have 2600 coins.")
        self.assertEqual(node1.get_balance(), 2600)
        print("DIfficulty for the last block should have been set to 4.")
        self.assertEqual(node1.blockchain.last_block.difficulty, 4)
        print("==================================================\n")

    def test_mine(self):
        print("==================================Mining test.")
        print("Creating 2 nodes.")
        node1 = Node('TES')
        node2 = Node('ZAN')

        print("Node 1 mines one block using the second nodes address.")
        node1.mine(node2.wallet.public_key)
        node2.blockchain.insert_block(node1.blockchain.last_block)

        print('This means that the mining reward should pass to Node 2.')
        self.assertEqual(node1.get_balance(), 0)
        self.assertEqual(node2.get_balance(), 100)
