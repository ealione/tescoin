from typing import Union, List, Set

from _exceptions import TransactionInsertionException, TransactionDoubleSpentWarning, InsufficientBalanceWarning, \
    InputSignatureException
from coin.transaction import Transaction
from config import TR_CAPACITY

RemovalData = Union[Transaction, Set[Transaction], List[Transaction]]


# TODO: Implement Merkle Root
class TransactionTree(object):
    def __init__(self, transaction_capacity=TR_CAPACITY):
        self.nodes = set()
        self.transaction_capacity = transaction_capacity

    def insert_transaction(self, value: Transaction) -> None:
        try:
            value.is_transaction_valid()
            self.is_double_spent(value)
            self.nodes.add(value)
        except (TransactionDoubleSpentWarning, InsufficientBalanceWarning, InputSignatureException):
            raise TransactionInsertionException()

    def insert_transactions(self, values: List[Transaction]) -> None:
        for transaction in values:
            try:
                self.insert_transaction(transaction)
            except TransactionInsertionException:
                raise TransactionInsertionException()

    def get_batch(self) -> List[Transaction]:
        transaction_list = list(self.nodes)[:self.transaction_capacity - 1]
        self.remove_transactions(transaction_list)
        return list(transaction_list)

    def is_double_spent(self, transaction: Transaction) -> None:
        if any(node.has_same_input(transaction) for node in self.nodes):
            raise TransactionDoubleSpentWarning()

    def remove_transactions(self, items: RemovalData) -> None:
        if not isinstance(items, set):
            items = set(items)
        self.nodes -= items

    def __contains__(self, item: Transaction) -> bool:
        return item in self.nodes

    def __len__(self):
        return len(self.nodes)
