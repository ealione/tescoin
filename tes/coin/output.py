import json
import hashlib


class Output(object):
    def __init__(self, address: str, amount: int) -> None:
        self.amount = amount
        self.address = address

    def __eq__(self, other):
        return self.hash == other.hash

    def __str__(self):
        return f'\nOutput' \
               f'\n-------' \
               f'\nTo     : {self.address}' \
               f'\nAmount : {self.amount}'

    def __repr__(self):
        return self._dump()

    def __bytes__(self):
        return self.__repr__().encode()

    def __hash__(self):
        return hash(self.__repr__())

    def _dump(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=2)

    @property
    def hash(self):
        return hashlib.sha256(self.__bytes__()).hexdigest()
