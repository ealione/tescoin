# -*- coding: utf-8 -*-
import binascii

import Crypto
import Crypto.Random
from Crypto.PublicKey import RSA


class Wallet(object):
    def __init__(self, owner):
        self.private_key = None
        self.public_key = None
        self.owner = owner
        self._generate_keys()

    def _generate_keys(self):
        # TODO: Store keys to file
        random_gen = Crypto.Random.new().read
        private_key = RSA.generate(1024, random_gen)
        public_key = private_key.publickey()
        self.private_key = binascii.hexlify(private_key.exportKey(format='DER')).decode('ascii')
        self.public_key = binascii.hexlify(public_key.exportKey(format='DER')).decode('ascii')

    @staticmethod
    def _to_file(private_key, public_key):
        with open("private.pem", "w") as f:
            print("{}".format(private_key.exportKey(format='DER')), file=f)

        with open("public.pem", "w") as f:
            print("{}".format(public_key.exportKey(format='DER')), file=f)

    @staticmethod
    def _from_file(private_keyfile, public_keyfile):
        with open(private_keyfile, 'rb') as f:
            private_key = binascii.hexlify(f.read()).decode('ascii')

        with open(public_keyfile, 'rb') as f:
            public_key = binascii.hexlify(f.read()).decode('ascii')

        return private_key, public_key
