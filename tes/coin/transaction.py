import hashlib
import json

from _exceptions import NoFeeTransactionWarning, InsufficientBalanceWarning, InputSignatureException
from config import TR_TYPE_REGULAR, TR_REWARD, TR_TYPE_FEE, TR_TYPE_REWARD
from coin.input import Input
from coin.output import Output


class Transaction(object):
    def __init__(self, inputs: [Input], outputs: [Output], transaction_type: str = TR_TYPE_REGULAR) -> None:
        self.inputs = inputs
        self.outputs = outputs
        self.type = transaction_type
        self._hash = self.compute_hash()

    @property
    def total_inputs(self) -> int:
        return sum(input.amount for input in self.inputs)

    @property
    def total_outputs(self) -> int:
        return sum(output.amount for output in self.outputs)

    def calculate_fee(self) -> int:
        if self.type == TR_TYPE_REGULAR:
            return self.total_inputs - self.total_outputs
        else:
            raise NoFeeTransactionWarning()

    def validate_total(self) -> None:
        if self.total_inputs < self.total_outputs:
            raise InsufficientBalanceWarning()

    def validate_inputs_signature(self) -> None:
        try:
            for input in self.inputs:
                input.verify()
        except InputSignatureException as e:
            raise e

    def has_same_input(self, other: 'Transaction') -> bool:
        return any(input in other for input in self.inputs)

    def is_transaction_valid(self) -> None:
        self.validate_total()
        self.validate_inputs_signature()

    @property
    def hash(self) -> str:
        return self._hash

    def compute_hash(self) -> str:
        return hashlib.sha256(
            json.dumps({
                'Inputs': repr(self.inputs),
                'Outputs': repr(self.outputs),
                'Type': self.type
            }, sort_keys=True).encode()).hexdigest()

    @staticmethod
    def reward_transaction(address: str) -> 'Transaction':
        output = Output(address, TR_REWARD)
        return Transaction([], [output], TR_TYPE_REWARD)

    def fee_transaction(self, address: str) -> 'Transaction':
        if self.__bool__():
            output = Output(address, self.calculate_fee())
            return Transaction([], [output], TR_TYPE_FEE)
        else:
            raise NoFeeTransactionWarning()

    def __str__(self):
        return f'\nTransaction ({self.type})' \
               f'\n----------------' \
               f'\nInputs  : {self.total_inputs} ({len(self.inputs)})' \
               f'\nOutputs : {self.total_outputs} ({len(self.outputs)})'

    def __eq__(self, other: 'Transaction'):
        return self.hash == other.hash

    def __repr__(self):
        return self._dump()

    def __bytes__(self):
        return self.__repr__().encode()

    def __bool__(self):
        try:
            self.is_transaction_valid()
            return True
        except (InsufficientBalanceWarning, InputSignatureException):
            return False

    def __contains__(self, item):
        if isinstance(item, Input):
            return item in self.inputs
        elif isinstance(item, Output):
            return item in self.outputs

    def __hash__(self):
        return hash(self.__repr__())

    def _dump(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=2)
