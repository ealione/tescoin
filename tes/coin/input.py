import json
import hashlib
import binascii

from Crypto.Hash import SHA
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5

from _exceptions import InputSignatureException


class Input(object):
    def __init__(self, index: int, transaction_hash: str, address: str, amount: int, signature: str = None):
        self.index = index
        self.transaction_hash = transaction_hash
        self.amount = amount
        self.address = address
        self.signature = signature
        self._hash = self.compute_hash()

    @property
    def is_signed(self):
        return self.signature is not None

    @property
    def hash(self) -> bytes:
        return self._hash

    def compute_hash(self) -> str:
        return hashlib.sha256(
            json.dumps({
                'Index': self.index,
                'Transaction Hash': self.transaction_hash,
                'Amount': repr(self.amount),
                'Address': self.address
            }, sort_keys=True).encode()).hexdigest()

    def sign(self, key: str) -> None:
        # TODO: Use the correct hash, optimize
        private_key = RSA.importKey(binascii.unhexlify(key))
        signer = PKCS1_v1_5.new(private_key)
        hash = SHA.new(self.hash.encode())
        self.signature = binascii.hexlify(signer.sign(hash)).decode('ascii')

    def verify(self):
        # TODO: Use the correct hash, optimize
        public_key = RSA.importKey(binascii.unhexlify(self.address))
        verifier = PKCS1_v1_5.new(public_key)
        hash = SHA.new(self.hash.encode())
        if not verifier.verify(hash, binascii.unhexlify(self.signature)):
            raise InputSignatureException()

    def __eq__(self, other):
        return self.transaction_hash == other.transaction_hash

    def __str__(self):
        return f'Input {"    (Signed)" if self.is_signed else "(Not Signed)"}\n' \
               f'\n------------------' \
               f'\nIndex              : {self.index}' \
               f'\nTransaction Hash:  : {self.transaction_hash}' \
               f'\nAddress            : {self.address}' \
               f'\nAmount             : {self.amount}'

    def __repr__(self):
        return self._dump()

    def __bytes__(self):
        return self.__repr__().encode()

    def __hash__(self):
        return hash((self.index, self.transaction_hash, self.address, self.amount))

    def _dump(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=2)