#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from argparse import ArgumentParser

from peer import Peer
from utils import rand_id
from _version import __version__


if __name__ == "__main__":
    parser = ArgumentParser(description='TES Coin v{}'.format(__version__))
    parser.add_argument("-n", "--name", type=str, default=rand_id(),
                        help="A name to identify your client. A unique random name will be chosen if blank.")
    parser.add_argument("-a", "--address", type=str, default='127.0.0.1',
                        help="The location of the client. Must be a valid IP address.")
    parser.add_argument("-t", "--transport", type=str, default='tcp',
                        choices=['tcp', 'udp'], help="The transport protocol to be used.")
    parser.add_argument("-p", "--port", type=int, default=4355,
                        help="The port to be used by the client. Must be between 1000 and 10000")

    args = parser.parse_args()

    peer = Peer(args.name, args.port, args.transport, args.address)
    peer.start()
