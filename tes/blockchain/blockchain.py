# -*- coding: utf-8 -*-
from blockchain.block import Block, BlockData

from _exceptions import DifficultyMismatchWarning, HashMismatchWarning, PreviousHashMismatchWarning, \
    BlockIndexMismatchWarning, InvalidBLockException, \
    BlockInsertionError, InvalidOriginWarning, ChainReplacementWarning, ChainSmallerWarning, ChainNotValidException


class BlockChain(object):
    def __init__(self, miner):
        self._blocks = [self.create_genesis_block()]
        self.miner = miner

    @staticmethod
    def create_genesis_block() -> Block:
        return Block(index=0, previous_hash="0", miner='TES', data=[], timestamp=797029200.0, nonce=0)

    @property
    def blocks(self) -> [BlockData]:
        return self._blocks

    @blocks.setter
    def blocks(self, value: 'BlockChain') -> None:
        self.resolve_conflict(value)

    @property
    def last_block(self) -> Block:
        return self.__getitem__(-1)

    @property
    def previous_block(self) -> Block:
        return self.__getitem__(-2)

    @property
    def genesis_block(self) -> Block:
        return self.__getitem__(0)

    def insert_block(self, block: Block) -> None:
        try:
            self.is_valid_block(block, self.last_block)
            self._blocks.append(block)
        except InvalidBLockException:
            raise BlockInsertionError()

    def generate_next_block(self, data: BlockData) -> None:
        new_block = Block.from_previous_block(self.last_block, self.miner, data)
        new_block.mine_block()
        self.insert_block(new_block)

    def resolve_conflict(self, value: 'BlockChain') -> None:
        self.should_replace_chain(value)
        self._blocks = value.blocks

    def should_replace_chain(self, chain: 'BlockChain'):
        try:
            self.is_same_origin(chain)
            self.is_chain_longer(chain)
            self.is_valid_chain(chain)
        except (InvalidOriginWarning, ChainSmallerWarning, ChainNotValidException):
            raise ChainReplacementWarning()

    def is_same_origin(self, chain: 'BlockChain') -> None:
        if self.genesis_block.hash != chain.genesis_block.hash:
            raise InvalidOriginWarning()

    def is_chain_longer(self, chain: 'BlockChain') -> None:
        if len(self) >= len(chain):
            raise ChainSmallerWarning()

    @staticmethod
    def is_valid_chain(chain: 'BlockChain') -> None:
        try:
            for index in range(1, len(chain)):
                block = chain[index]
                previous_block = chain[index - 1]
                chain.is_valid_block(block, previous_block)
        except InvalidBLockException:
            raise ChainNotValidException()

    @property
    def is_valid(self) -> bool:
        try:
            self.is_valid_chain(self)
            return True
        except ChainNotValidException:
            return False

    def is_valid_block(self, block: Block, previous_block: Block) -> None:
        try:
            self.is_valid_index(block, previous_block)
            self.is_valid_previous_hash(block, previous_block)
            self.is_valid_hash(block)
            self.is_valid_difficulty(block)
        except (BlockIndexMismatchWarning, PreviousHashMismatchWarning, HashMismatchWarning, DifficultyMismatchWarning):
            raise InvalidBLockException()

    @staticmethod
    def is_valid_index(block: Block, previous_block: Block) -> None:
        if block.index != previous_block.index + 1:
            raise BlockIndexMismatchWarning()

    @staticmethod
    def is_valid_previous_hash(block: Block, previous_block: Block) -> None:
        if block.previous_hash != previous_block.hash:
            raise PreviousHashMismatchWarning()

    @staticmethod
    def is_valid_hash(block: Block) -> None:
        if block.hash != block.compute_hash():
            raise HashMismatchWarning()

    @staticmethod
    def is_valid_difficulty(block: Block) -> None:
        if not block:
            raise DifficultyMismatchWarning()

    def __getitem__(self, index: int) -> [Block, None]:
        try:
            return self._blocks[index]
        except IndexError:
            return None

    def __len__(self) -> int:
        return len(self.blocks)


if __name__ == "__main__":
    import doctest

    doctest.testmod()
