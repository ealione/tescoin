# -*- coding: utf-8 -*-
import hashlib
import json
import sys
from time import time, localtime, strftime
from typing import List

from coin.transaction import Transaction
from _exceptions import HashDifficultyException
from utils import readable_bytes

BlockData = List[Transaction]


class Block(object):
    def __init__(self, index: int, previous_hash: str, miner: str, data: BlockData = None, timestamp: float = None,
                 nonce: int = 0) -> None:
        self.index = index
        self.previous_hash = previous_hash
        self.data = data or []
        self.timestamp = timestamp or time()
        self.nonce = nonce
        self.miner = miner
        self.main_chain = False
        self.orphan = True
        self.next_hash = None
        self.hash = self.compute_hash()

    @classmethod
    def from_previous_block(cls, previous_block, miner: str, new_data: BlockData = None) -> 'Block':
        return cls(previous_block.index + 1, previous_block.hash, miner, new_data)

    @property
    def data_size(self) -> float:
        return sys.getsizeof(self.data)

    @property
    def transaction_count(self) -> int:
        """Returns the number of items held by the `Block`.

        :rtype: int
        """
        return len(self.data)

    def mine_block(self) -> None:
        while not self:
            self.nonce += 1
            self.hash = self.compute_hash()

    def compute_hash(self) -> str:
        return hashlib.sha256(
            json.dumps({
                'Block': self.index,
                'Previous': self.previous_hash,
                'Transactions': repr(self.data),
                'Nonce': self.nonce,
                'Timestamp': self.timestamp,
                'Miner': self.miner
            }, sort_keys=True).encode()).hexdigest()

    @property
    def difficulty(self):
        return round(self.index / 50) + 3

    def valid_difficulty(self, hash: str) -> None:
        if hash[:self.difficulty] != '0' * self.difficulty:
            raise HashDifficultyException()

    def __bool__(self) -> bool:
        try:
            self.valid_difficulty(self.hash)
            return True
        except HashDifficultyException:
            return False

    def __str__(self):
        return f'*===============================================================[Block {self.index:10d}]==*' \
               f'\n    Previous Hash : {self.previous_hash}' \
               f'\n     Current Hash : {self.hash}' \
               f'\n        Timestamp : {strftime("%Y-%m-%d %H:%M:%S", localtime(self.timestamp))}' \
               f'\n            Bytes : {readable_bytes(self.data_size)}' \
               f'\nTransaction Count : {self.transaction_count}' \
               f'\n            Valid : {self.__bool__()}' \
               f'\n         Mined by : {self.miner}' \
               f'\n     Transactions : {self.data}'

    def __repr__(self):
        return self._dump()

    def __bytes__(self):
        return self.__repr__().encode()

    def __eq__(self, other):
        return self.hash == other.hash

    def __len__(self):
        return len(self.data)

    def _dump(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=2)


if __name__ == "__main__":
    import doctest

    doctest.testmod()
