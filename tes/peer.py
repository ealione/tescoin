import ipaddress
import readline

import zmq

from _exceptions import BlockInsertionError, ChainReplacementWarning
from config import PEER_PROTO, MSG_READY, MSG_LATEST_BLOCK, MSG_BLOCKCHAIN, MSG_TRANSACTIONS, MSG_DISCOVER_PEERS
from console.interactive_console import HistoryConsole
from node import Node
from utils import zmq_addr, threaded, split_address, serialize_object, deserialize_object

readline.parse_and_bind('tab: complete')
readline.parse_and_bind('set editing-mode vi')
# https://pymotw.com/2/readline/


class Peer(object):
    def __init__(self, name, port, transport=None, address=None):
        self.main_enpoint = zmq_addr(port, transport, address).encode()
        self.main_stream = None
        self.local_stream = None
        self.name = name.encode()
        self.peers = {}
        self.node = Node(name)
        self.context = None
        self.socket = None
        self.usr_cmds = {
            'connect': self.connect_to_peer,
            'help': self.print_help,
            'balance': self.balance,
            'peers': self.plot_peers,
            't': self.transaction,
            'view': self.view_last_block,
            'mine': self.mine
        }
        self.peer_cmds = {
            MSG_READY: self.on_ready,
            MSG_LATEST_BLOCK: self.on_latest_block,
            MSG_TRANSACTIONS: self.on_transactions
        }
        self.listen()

    # TODO: handle multiple payments and fees
    def transaction(self, args):
        payment = self.payment(args[0], args[1])
        self.node.transaction([payment])

    # TODO: throw error instead of if check
    def payment(self, peer_name, amount):
        for peer in self.peers.values():
            if peer.name == peer_name.encode():
                address = peer.public_key
                return self.node.payment(address, int(amount))
            else:
                raise IndexError

    def view_last_block(self, args):
        print(self.node.blockchain.last_block)

    def mine(self, args):
        self.node.mine()

    @threaded
    def _send_message_to(self, endpoint, message):
        self.local_stream = self.context.socket(zmq.DEALER)
        self.local_stream.connect(endpoint)
        self.local_stream.send_multipart(message)
        self.local_stream.close()

    def send_ready(self, endpoint, type=b'hi'):
        ready_msg = [b'', PEER_PROTO, MSG_READY, type, self.main_enpoint, self.name,
                     self.node.wallet.public_key.encode()]
        self._send_message_to(endpoint, ready_msg)
        return

    @threaded
    def listen(self):
        self.context = zmq.Context().instance()
        self.socket = self.context.socket(zmq.ROUTER)
        self.socket.setsockopt(zmq.LINGER, 0)
        self.socket.bind(self.main_enpoint)
        while True:
            try:
                self.on_message(self.socket.recv_multipart())
            except (KeyboardInterrupt, zmq.ContextTerminated):
                break

    def send_latest_block(self, name):
        to_send = [b'', PEER_PROTO, MSG_LATEST_BLOCK, serialize_object(self.node.blockchain.last_block)]
        self.peers[name].send_message(to_send)

    def publish_latest_block(self):
        to_send = [b'', PEER_PROTO, MSG_LATEST_BLOCK, serialize_object(self.node.blockchain.last_block)]
        self.broadcast(to_send)

    # TODO: make sure that we also send our name
    def on_latest_block(self, msg):
        name = msg.pop(0)
        last_block = deserialize_object(msg.pop(0))
        try:
            self.node.blockchain.insert_block(last_block)
            self.publish_latest_block()
        except BlockInsertionError:
            print("Received invalid block!")
        finally:
            self.request_blockchain(name)

    def on_blockchain(self, msg):
        try:
            chain = deserialize_object(msg.pop(0))
            res = self.node.blockchain.resolve_conflict(chain)
            self.publish_latest_block()
        except ChainReplacementWarning:
            print("Sticking to local chain!")

    def request_blockchain(self, name):
        to_send = [b'', PEER_PROTO, self.name, MSG_BLOCKCHAIN]
        try:
            self.peers[name].send_message(to_send)
        except KeyError:
            print(f'Node {name} not registered!')

    def send_transactions(self, name):
        to_send = [b'', PEER_PROTO, MSG_TRANSACTIONS, serialize_object(self.node.transaction_tree.nodes)]
        self.peers[name].send_message(to_send)

    def on_transactions(self, msg):
        transactions = deserialize_object(msg.pop(0))
        self.node.transaction_tree.nodes = transactions

    def connect_to_peer(self, msg):
        try:
            address = msg.pop(0)
            port = int(msg.pop(0))
            if address != 'localhost':
                ipaddress.ip_address(address)
            endpoint = zmq_addr(port, 'tcp', address)
            self.send_ready(endpoint)
        except (ValueError, TypeError, IndexError):
            print('Could not parse connection arguments!')
        except AssertionError:
            print("Will not accept a port les than 1000 or larger than 10000!")

    def disconnect_from_peer(self, msg):
        try:
            name = msg.pop(0)
            del self.peers[name]
        except IndexError:
            print('Node is not known!')

    def on_message(self, msg):
        rs, msg = split_address(msg)
        # TODO: check protocol
        protocol = msg.pop(0)
        cmd = msg.pop(0)
        if cmd in self.peer_cmds:
            fnc = self.peer_cmds[cmd]
            fnc(msg)

    def on_ready(self, msg):
        msg_type = msg.pop(0)
        endpoint = msg.pop(0)
        name = msg.pop(0)
        pkey = msg.pop(0)
        # TODO: if name exists then add a +1 into it
        p_name = self.endpoint_registered(endpoint)
        if p_name:
            self.peers[name] = self.peers.pop(p_name)
        else:
            self.peers[name] = Neighbour(endpoint, self.context, name, pkey)
        print(f'{name.decode()} says hi!')
        if msg_type == b'hi':
            self.send_ready(endpoint, type=b'nmt')

    def on_remove_transaction(self, msg):
        transaction = deserialize_object(msg.pop(0))
        self.node.transaction_tree.remove(transaction)

    def endpoint_registered(self, endpoint):
        for peer in self.peers.values():
            if endpoint == peer.endpoint:
                return peer.name
        return None

    # TODO: does this work
    def send_peer_list(self, name):
        peers = {}
        for i, peer in enumerate(self.peers.values()):
            peers[peer.name] = {'endpoint': peer.endpoint, 'pk': peer.public_key}
        to_send = [b'', MSG_DISCOVER_PEERS, serialize_object(peers)]

    def start(self):
        try:
            hc = HistoryConsole(self.parse_input)
            hc.interact()
        except (KeyboardInterrupt, zmq.ContextTerminated):
            pass
        finally:
            self.shutdown()

    @staticmethod
    def print_help(args):
        print("Available commands:\n"
              "peers\t\t get list of available nodes.\n"
              "connect <address> <port>\t\t connect to a node.\n"
              "t <recipient_address> <amount>\t make a transaction.\n"
              "balance\t\t view available balance.\n"
              "view\t\t view last transactions.\n"
              "mine\t\t mine new block.\n"
              "help\t\t display this message.")

    def plot_peers(self, args):
        print(list(self.peers.keys()))

    def balance(self, args):
        if len(args):
            peer = args.pop(0)
            if peer in self.peers:
                address = self.peers[peer].public_key
                name = self.peers[peer].name
            else:
                print(f'{peer} is not recognized as a peer.')
                return
        else:
            address = self.node.wallet.public_key
            name = self.name
        print(f'Available founds for {name} are {self.node.get_balance(address)} coins')

    def parse_input(self, user_input):
        try:
            cmd, *args = user_input.split()
        except ValueError:
            cmd, args = user_input, []
        try:
            fnc = self.usr_cmds[cmd]
            fnc(args)
        except KeyError:
            print('Input not recognized!')

    def broadcast(self, msg):
        for peer in self.peers.values():
            peer.send_message(msg)

    def shutdown(self):
        for peer in self.peers.values():
            peer.shutdown()
        self.socket.close()
        self.context.term()


class Neighbour(object):
    def __init__(self, endpoint, context, name=None, public_key=None):
        self.endpoint = endpoint
        self.name = name
        self.context = context
        self.public_key = public_key
        self.stream = None
        self._create_stream()

    def _create_stream(self):
        self.stream = self.context.socket(zmq.DEALER)
        self.stream.connect(self.endpoint)

    # TODO: some connection is not being killed here
    def shutdown(self):
        if self.stream is not None:
            self.stream.close()

    def send_message(self, msg):
        if self.stream.closed:
            self.shutdown()
        self.stream.send_multipart(msg)


if __name__ == "__main__":
    pass
