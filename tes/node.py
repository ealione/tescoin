from blockchain.blockchain import BlockChain
from coin.transaction_tree import TransactionTree
from coin.wallet import Wallet
from coin.payment import Payment
from coin.input import Input
from coin.output import Output
from coin.transaction import Transaction

from _exceptions import NoFeeTransactionWarning, BlockInsertionError
from config import TR_REWARD, TR_TYPE_REWARD, TR_TYPE_FEE


class Node(object):
    def __init__(self, name: str, block_capacity: int = 5) -> None:
        self.transaction_tree = TransactionTree(block_capacity)
        self.blockchain = BlockChain(name)
        self.wallet = Wallet(name)

    def mine(self, address: str = None) -> None:
        address = address or self.wallet.public_key
        regular_transactions = self.transaction_tree.get_batch()
        reward_transaction = self.create_reward_transaction(address)
        transactions = regular_transactions + reward_transaction
        try:
            fee_transaction = self.create_fee_transaction(regular_transactions, address)
            transactions.append(fee_transaction)
        except NoFeeTransactionWarning:
            print('Transactions incur no fee.')
        finally:
            try:
                self.blockchain.generate_next_block(transactions)
            except BlockInsertionError:
                print('Could not insert new block.')

    def transaction(self, payments: [Payment]) -> Transaction:
        total_payment = sum(payment.amount for payment in payments)
        total_fee = sum(payment.fee for payment in payments)
        unspent_inputs = self.get_unspent_inputs()

        input_total = 0
        inputs = []
        for output in unspent_inputs:
            if input_total < total_payment + total_fee:
                input_total += output.amount
                inputs.append(output)

        outputs = []
        for payment in payments:
            outputs.append(Output(payment.address, payment.amount))

        change = input_total - total_payment - total_fee

        if change > 0:
            outputs.append(Output(self.wallet.public_key, change))

        self.sign_inputs(inputs)

        return Transaction(inputs, outputs)

    @staticmethod
    def payment(address: str, amount: int, fee: int = 5) -> Payment:
        return Payment(address, amount, fee)

    def get_balance(self, address: str = None) -> int:
        address = address or self.wallet.public_key
        inputs = self.get_unspent_inputs()
        filtered_inputs = [input for input in inputs if input.address == address]
        return sum(input.amount for input in filtered_inputs)

    def sign_inputs(self, inputs: [Input]) -> None:
        for input in inputs:
            input.sign(self.wallet.private_key)

    def get_unspent_inputs(self) -> [Input]:
        inputs = self.get_inputs()
        outputs = self.get_outputs()
        unspent_inputs = [output for output in outputs if output not in inputs]
        return unspent_inputs

    def get_inputs(self) -> [Input]:
        inputs = []
        for block in self.blockchain.blocks:
            for transaction in block.data:
                for input in transaction.inputs:
                    if input.address == self.wallet.public_key:
                        inputs.append(input)
        return inputs

    def get_outputs(self) -> [Output]:
        outputs = []
        for block in self.blockchain.blocks:
            for i, transaction in enumerate(block.data):
                for output in transaction.outputs:
                    if output.address == self.wallet.public_key:
                        outputs.append(Input(i, transaction.hash, output.address, output.amount))
        return outputs

    @staticmethod
    def create_reward_transaction(address: str) -> [Transaction]:
        output = Output(address, TR_REWARD)
        return [Transaction([], [output], TR_TYPE_REWARD)]

    @staticmethod
    def create_fee_transaction(transactions: [Transaction], address: str) -> [Transaction]:
        # total_fee = sum(transaction.calculate_fee for transaction in transactions)
        total_fee = 0
        for transaction in transactions:
            try:
                total_fee += transaction.calculate_fee()
            except NoFeeTransactionWarning:
                pass
        if total_fee > 0:
            output = Output(address, total_fee)
            return Transaction([], [output], TR_TYPE_FEE)
        else:
            raise NoFeeTransactionWarning()
