import atexit
import os
import readline

from _exceptions import CommandNotRecognized, CommandIncomplete


class InteractiveConsole:
    def __init__(self, callback):
        self.buffer = []
        self.resetbuffer()
        self.runcode = callback

    def resetbuffer(self):
        self.buffer = []

    def interact(self, banner=None, exitmsg=None):
        ps1 = "||| "
        ps2 = "... "
        cprt = 'Type "help", "copyright", "credits" or "license" for more information.'
        if banner is None:
            print("Tescoin")
        elif banner:
            print(f"{str(banner)}\n")
        more = 0
        while 1:
            try:
                if more:
                    prompt = ps2
                else:
                    prompt = ps1
                try:
                    line = self.raw_input(prompt)
                except EOFError:
                    print("\n")
                    break
                else:
                    more = self.push(line)
            except KeyboardInterrupt:
                print("\nKeyboardInterrupt")
                self.resetbuffer()
                more = 0
                break
        if exitmsg is None:
            print('Exiting\n')
        elif exitmsg != '':
            print(f'{exitmsg}\n')

    def push(self, line):
        self.buffer.append(line)
        source = "\n".join(self.buffer)
        more = self.runsource(source)
        if not more:
            self.resetbuffer()
        return more

    @staticmethod
    def raw_input(prompt=""):
        return input(prompt)

    def runsource(self, source):
        try:
            self.runcode(source)
        except CommandNotRecognized:
            print("Command was not recognized!")
            return False
        except CommandIncomplete:
            return True

        return False


class HistoryConsole(InteractiveConsole):
    def __init__(self, callback, histfile=os.path.join(os.path.expanduser(".."), "../.console_history"), vocab=None):
        InteractiveConsole.__init__(self, callback)
        self.init_history(histfile)
        self.init_completer(vocab)

    def init_completer(self, vocab):
        completer = HistoryCompleter(vocab)
        readline.set_completer(completer.complete)

    def init_history(self, histfile):
        readline.parse_and_bind("tab: complete")
        readline.parse_and_bind('set editing-mode vi')
        if hasattr(readline, "read_history_file"):
            try:
                readline.read_history_file(histfile)
                h_len = readline.get_current_history_length()
            except FileNotFoundError:
                open(histfile, 'wb').close()
                h_len = 0
            atexit.register(self.save_history, h_len, histfile)

    @staticmethod
    def save_history(prev_h_len, histfile):
        new_h_len = readline.get_current_history_length()
        readline.set_history_length(1000)
        readline.append_history_file(new_h_len - prev_h_len, histfile)


class HistoryCompleter:
    def __init__(self, vocab):
        self.vocab = vocab

    def complete(self, text, state):
        results = [str(readline.get_history_item(i + 1)) for i in range(readline.get_current_history_length()) if
                   str(readline.get_history_item(i + 1)).startswith(text)] + self.vocab
        return results[state]


if __name__ == "__main__":
    hc = HistoryConsole(print, vocab=['testing'])
    hc.interact()
