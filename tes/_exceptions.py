class DifficultyMismatchWarning(UserWarning):
    pass


class PreviousHashMismatchWarning(UserWarning):
    pass


class BlockIndexMismatchWarning(UserWarning):
    pass


class HashMismatchWarning(UserWarning):
    pass


class InvalidBLockException(Exception):
    pass


class BlockInsertionError(ValueError):
    pass


class InvalidOriginWarning(UserWarning):
    pass


class ChainReplacementWarning(UserWarning):
    pass


class ChainSmallerWarning(UserWarning):
    pass


class ChainNotValidException(Exception):
    pass


class HashDifficultyException(Exception):
    pass


class InputSignatureException(Exception):
    pass


class NoFeeTransactionWarning(UserWarning):
    pass


class InsufficientBalanceWarning(UserWarning):
    pass


class TransactionInsertionException(Exception):
    pass


class TransactionDoubleSpentWarning(UserWarning):
    pass


class CommandNotRecognized(Exception):
    pass


class CommandIncomplete(Exception):
    pass
